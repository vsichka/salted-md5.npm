# Salted MD5

[![NPM Downloads][downloads-image]][downloads-url]

<img src="https://i.imgur.com/aAytvXv.png" width="200" />

## Description

Generate salted MD5 hashes to enhance resistance against hash cracking and rainbow table attacks.

## Installation

```sh
npm install salted-md5
```

## Using

```js
// Import.
const saltedMd5 = require('salted-md5');

// Get salted MD5.
const saltedHash = saltedMd5('Some data.', 'SUPER-S@LT!');
// Value of "saltedHash": "1b25469a2e4b157f02dd1fea3b5f1e9a".
```

## License

The MIT License (MIT)

Copyright © 2016 - 2025 Volodymyr Sichka

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

[downloads-image]: https://img.shields.io/npm/dm/salted-md5.svg
[downloads-url]: https://npmjs.org/package/salted-md5
